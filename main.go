// Hpdata reads the FOCUS PC Client String and generates input data records
// for the Hypack navigation software.
package main

import (
	"bytes"
	"encoding/binary"
	"flag"
	"fmt"
	"log"
	"math"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"text/template"

	"bitbucket.org/mfkenney/go-nmea"
	focus "bitbucket.org/uwaploe/go-focus"
	"bitbucket.org/uwaploe/navsvc/pkg/atlas"
	"bitbucket.org/uwaploe/navsvc/pkg/boss"
	stan "github.com/nats-io/go-nats-streaming"
)

const Usage = `Usage: hpdata [options]

Send custom data to Hypack.
`

var Version = "dev"
var BuildDate = "unknown"

// Format of time field in GLL sentence
const TIME_FMT = "150405.00"

const FORMAT = `FISHZ,{{if .depth}}{{.depth|printf "%.1f"}}{{end}},{{if .cableLength}}{{.cableLength|printf "%.1f"}}{{end}}`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	dataFmt             = flag.String("fmt", FORMAT, "Output data format template")
	natsURL      string = "nats://localhost:4222"
	clusterID    string = "must-cluster"
	focusSubject string = "nmea.focus"
	navSubject   string = "nav.data"
	qLen         int    = 10
)

func todm(d float64) (int, float64) {
	deg := int(d)
	min := (d - float64(deg)) * 60
	return deg, min
}

func createGLL(rec atlas.MustNavMessage) nmea.Sentence {
	s := nmea.Sentence{}
	s.Id = "GPGLL"
	s.Fields = make([]string, 7)

	d, m := todm(float64(rec.Lat) * 180. / math.Pi)
	if d < 0 || m < 0 {
		s.Fields[0] = fmt.Sprintf("%02d%07.4f", -d, -m)
		s.Fields[1] = "S"
	} else {
		s.Fields[0] = fmt.Sprintf("%02d%07.4f", d, m)
		s.Fields[1] = "N"
	}

	d, m = todm(float64(rec.Lon) * 180. / math.Pi)
	if d < 0 || m < 0 {
		s.Fields[2] = fmt.Sprintf("%03d%07.4f", -d, -m)
		s.Fields[3] = "W"
	} else {
		s.Fields[2] = fmt.Sprintf("%03d%07.4f", d, m)
		s.Fields[3] = "E"
	}

	s.Fields[4] = rec.T.AsTime().UTC().Format(TIME_FMT)
	s.Fields[5] = "A"
	s.Fields[6] = "A"

	return s
}

func createHDT(rec atlas.MustNavMessage) nmea.Sentence {
	s := nmea.Sentence{}
	s.Id = "GPHDT"
	s.Fields = make([]string, 2)

	s.Fields[0] = fmt.Sprintf("%.2f", float64(rec.Heading)*180./math.Pi)
	s.Fields[1] = "T"

	return s
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&focusSubject, "focus-sub", lookupEnvOrString("FOCUS_SUBJECT", focusSubject),
		"Subject name for FOCUS data")
	flag.StringVar(&navSubject, "nav-sub", lookupEnvOrString("NAV_SUBJECT", navSubject),
		"Subject name for navigation data")
	flag.IntVar(&qLen, "qlen", qLen, "Size of nav data record queue")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	parseCmdLine()

	focusPort := os.Getenv("HYPACK_FOCUS_PORT")
	navPort := os.Getenv("HYPACK_NAV_PORT")

	if focusPort == "" && navPort == "" {
		log.Fatalln("At least one of HYPACK_FOCUS_PORT or HYPACK_NAV_PORT must be set")
	}

	tmpl := template.Must(template.New("rec").Parse(*dataFmt))

	var (
		pfocus, pnav Port
		err          error
	)

	hpBaud, err := strconv.ParseInt(os.Getenv("HYPACK_BAUD"), 10, 32)
	if err != nil {
		log.Fatalln("HYPACK_BAUD env variable not set")
	}

	if focusPort != "" {
		if strings.Contains(focusPort, ":") {
			pfocus, err = NetworkPort(focusPort)
		} else {
			pfocus, err = SerialPort(focusPort, int(hpBaud))
		}
	}

	if navPort != "" {
		if strings.Contains(navPort, ":") {
			pnav, err = NetworkPort(navPort)
		} else {
			pnav, err = SerialPort(navPort, int(hpBaud))
		}
	}

	sc, err := stan.Connect(clusterID, "gen-hpdata", stan.NatsURL(natsURL))
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}
	defer sc.Close()

	focusCb := func(m *stan.Msg) {
		var sentence string
		// Accept tagged or untagged NMEA messages produced by nmeapub.
		bs := bytes.SplitN(m.Data, []byte(":"), 2)
		if len(bs) == 2 {
			sentence = string(bs[1])
		} else {
			sentence = string(bs[0])
		}
		rec, err := focus.ParseRecord(sentence)
		if err != nil {
			log.Printf("FOCUS data parse error: %v", err)
			return
		}

		if pfocus != nil {
			tmpl.Execute(pfocus, rec)
			pfocus.Write([]byte("\r\n"))
		}
	}

	var focusSub, navSub stan.Subscription

	if pfocus != nil {
		focusSub, err = sc.Subscribe(focusSubject, focusCb)
		if err != nil {
			log.Fatalf("Cannot subscribe to FOCUS data: %v", err)
		}
	}

	// Create a worker to extract nav message data
	queue := make(chan atlas.MustNavMessage, 10)
	defer close(queue)

	go func() {
		for rec := range queue {
			if pnav != nil {
				fmt.Fprintf(pnav, "%s\r\n", createGLL(rec))
				if (rec.Valid & atlas.ValidHeading) == atlas.ValidHeading {
					fmt.Fprintf(pnav, "%s\r\n", createHDT(rec))
				}
			}
		}
	}()

	// The navigation data is sent in JSF format, we need to skip the
	// header and the padding
	hdrlen := binary.Size(boss.Header{})
	pad := binary.Size(boss.JsfEdpPadding{})

	navCb := func(m *stan.Msg) {
		rec := atlas.MustNavMessage{}
		err := binary.Read(bytes.NewReader(m.Data[hdrlen+pad:]), boss.ByteOrder, &rec)
		if err != nil {
			log.Printf("Cannot parse nav message: %v", err)
			return
		}
		select {
		case queue <- rec:
		default:
			log.Println("WARNING: output queue full")
		}
	}

	if pnav != nil {
		navSub, err = sc.Subscribe(navSubject, navCb)
		if err != nil {
			log.Fatalf("Cannot subscribe to nav data: %v", err)
		}
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	done := make(chan bool)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			if pfocus != nil {
				focusSub.Unsubscribe()
			}
			if pnav != nil {
				navSub.Unsubscribe()
			}
			done <- true
		}
	}()

	log.Printf("HYPACK data service starting %s", Version)

	<-done
}
