module bitbucket.org/uwaploe/hpdata

require (
	bitbucket.org/mfkenney/go-nmea v1.4.0
	bitbucket.org/uwaploe/go-focus v0.4.1
	bitbucket.org/uwaploe/navsvc v1.7.0
	github.com/nats-io/go-nats-streaming v0.4.4
	github.com/nats-io/nats-server v1.4.1 // indirect
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
)

go 1.13
